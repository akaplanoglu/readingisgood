package com.getir.readinggood.config;

import static java.lang.String.format;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import java.util.Collection;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

@Configuration
@EnableAutoConfiguration(exclude = {EmbeddedMongoAutoConfiguration.class})
public class MongoConfig extends AbstractMongoClientConfiguration {


	@Value("${product.mongo.host}")
	private String mongoHost;

	@Value("${product.mongo.port}")
	private String mongoPort;

	@Value("${product.mongo.database}")
	private String mongoDatabase;

	@Value("${product.mongo.username}")
	private String mongoUsername;

	@Value("${product.mongo.password}")
	private String mongoPassword;


	@Override
	protected String getDatabaseName() {
		return mongoDatabase;
	}

	@Override
	public MongoClient mongoClient() {

		String connection = format(
				"mongodb://%s:%s@%s:%s/%s",
				mongoUsername,
				mongoPassword,
				mongoHost,
				mongoPort,
				mongoDatabase
		);
		ConnectionString connectionString = new ConnectionString(connection);
		MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
				.applyConnectionString(connectionString)
				.build();

		return MongoClients.create(mongoClientSettings);
	}

	@Override
	public Collection getMappingBasePackages() {
		return Collections.singleton("com.getir");
	}

}
