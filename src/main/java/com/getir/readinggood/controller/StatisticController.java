package com.getir.readinggood.controller;

import static org.springframework.http.ResponseEntity.ok;

import com.getir.readinggood.service.StatisticService;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/statistic")
@Validated
public class StatisticController {

	@Autowired
	private StatisticService statisticService;


	@GetMapping("/{userId}")
	public ResponseEntity getMonthlyStatsByUserId(@PathVariable("userId") @NotEmpty String userId) {
		return ok(statisticService.getUsersLastNMonthStats(userId));
	}

}
