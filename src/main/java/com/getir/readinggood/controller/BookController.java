package com.getir.readinggood.controller;

import com.getir.readinggood.model.Book;
import com.getir.readinggood.model.Order;
import com.getir.readinggood.service.BookService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/book")
@Validated
public class BookController {

	@Autowired
	BookService bookService;

	@PostMapping(value = "/save")
	public ResponseEntity<?> saveBook(@Valid @RequestBody Book book) {

		return new ResponseEntity(bookService.save(book), HttpStatus.OK);
	}


	@PutMapping(value = "/updateStock")
	public ResponseEntity<?> saveOrUpdateBook(@Valid @RequestBody Order order) {

		return new ResponseEntity(bookService.updateStock(order), HttpStatus.OK);
	}

}
