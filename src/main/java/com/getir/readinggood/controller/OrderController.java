package com.getir.readinggood.controller;

import com.getir.readinggood.model.Order;
import com.getir.readinggood.service.OrderService;
import java.util.Date;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/order")
@Validated
public class OrderController {

	@Autowired
	OrderService orderService;

	@GetMapping("/{id}")
	public ResponseEntity<Order> getOrderById(@PathVariable("id") @NotEmpty String id) {
		return new ResponseEntity(orderService.findById(id), HttpStatus.OK);
	}

	@PostMapping(value = "/save")
	public ResponseEntity<?> saveOrder(@Valid @RequestBody Order order) throws Exception {

		return new ResponseEntity(orderService.save(order), HttpStatus.OK);
	}

	@GetMapping(value = "/dates/{from}/{to}")
	public ResponseEntity<?> getOrdersByDate(@PathVariable("from") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fromDate,
			@PathVariable("to") @DateTimeFormat(pattern = "yyyy-MM-dd") Date toDate) {

		return new ResponseEntity(orderService.findOrdersByDate(fromDate, toDate), HttpStatus.OK);
	}

}
