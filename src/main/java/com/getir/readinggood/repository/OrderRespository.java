package com.getir.readinggood.repository;

import com.getir.readinggood.model.Order;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRespository extends MongoRepository<Order, String> {

	List<Order> findByCreatedDateBetween(Date startDate, Date endDate);

}
