package com.getir.readinggood.model;

import lombok.Data;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "roles")
@Data
public class Role extends BaseModel {


	@Indexed(unique = true, direction = IndexDirection.DESCENDING)
	private String role;


}
