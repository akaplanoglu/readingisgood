package com.getir.readinggood.model.constant;

public enum OrderStatus {
	NEW,
	IN_PROGRESS,
	COMPLETED
}
