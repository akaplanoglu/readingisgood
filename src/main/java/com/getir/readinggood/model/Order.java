package com.getir.readinggood.model;

import static org.springframework.data.mongodb.core.mapping.FieldType.DECIMAL128;

import com.getir.readinggood.model.constant.OrderStatus;
import java.math.BigDecimal;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@EqualsAndHashCode(callSuper = false)
@Data
@Document(collection = "orders")
public class Order extends BaseModel {

	private OrderStatus orderStatus;

	@Min(value = 1, message = "Order count should be greater than 0")
	private int orderCount;

	@NotNull(message = "Book should not be empty")
	private Book book;

	@Indexed
	@NotNull(message = "User id should not be empty")
	private String userId;

	@Field(targetType = DECIMAL128)
	private BigDecimal orderTotalPrice;

}
