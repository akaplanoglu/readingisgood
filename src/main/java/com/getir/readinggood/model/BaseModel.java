package com.getir.readinggood.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;

@Data
public abstract class BaseModel {

	@Id
	private String id;

	@JsonIgnore
	private boolean deleted = false;

	@JsonIgnore
	private boolean updated = false;

	@JsonIgnore
	private  boolean isActive = false;

	@JsonIgnore
	@CreatedDate
	@Indexed
	private Date createdDate;

	@JsonIgnore
	@LastModifiedDate
	private Date updatedDate;

	@CreatedBy
	private String createdBy;

	@LastModifiedBy
	private String lastModifiedBy;


}
