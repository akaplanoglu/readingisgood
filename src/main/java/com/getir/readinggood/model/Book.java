package com.getir.readinggood.model;

import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@EqualsAndHashCode(callSuper = false)
@Data
@Document(collection = "books")
public class Book extends BaseModel {


	@Indexed(unique = true)
	@NotEmpty(message = "Please provide a book name")
	private String name;

	@Min(value = 0, message = "Stock count should be greater than -1")
	private int stockCount;

	@NotNull(message = "Please provide a price")
	@DecimalMin("0.00")
	private BigDecimal price;

}
