package com.getir.readinggood.model;

import java.util.Set;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@EqualsAndHashCode(callSuper = false)
@Data
@Document(collection = "users")
public class User extends BaseModel {

	private String username;

	private String password;

	private String email;

	@DBRef
	private Set<Role> roles;

	private boolean enabled;

}
