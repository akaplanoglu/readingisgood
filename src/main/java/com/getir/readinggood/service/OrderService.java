package com.getir.readinggood.service;

import com.getir.readinggood.model.Order;
import java.util.Date;
import java.util.List;
import org.springframework.util.StringUtils;

public interface OrderService {

	Order save(Order order) throws Exception;

	Order findById(String orderId);

	List<Order> findOrdersByDate(Date startDate, Date endDate);
}
