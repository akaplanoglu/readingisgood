package com.getir.readinggood.service;

import com.getir.readinggood.dto.response.UserOrderMonthlyStatsResponse;
import java.util.List;

public interface StatisticService {

	List<UserOrderMonthlyStatsResponse> getUsersLastNMonthStats(String userId);

}
