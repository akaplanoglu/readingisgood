package com.getir.readinggood.service.impl;

import com.getir.readinggood.model.Book;
import com.getir.readinggood.model.Order;
import com.getir.readinggood.repository.BookRepository;
import com.getir.readinggood.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	BookRepository bookRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public Book save(Book book) {
		return bookRepository.save(book);
	}

	/**
	 * This method uses findAndModify to lock all transactions atomically in mongodb.
	 *
	 * @return null when there is no available stock.
	 */
	@Override
	public Book updateStock(Order order) {

		Query query = new Query(Criteria.where("_id").is(order.getBook().getId()).and("stockCount")
				.gte(order.getOrderCount()));

		Update update = new Update();
		update.inc("stockCount", -order.getOrderCount());
		FindAndModifyOptions options = new FindAndModifyOptions();
		options.upsert(false);
		options.returnNew(true);

		return mongoTemplate.findAndModify(query, update, options, Book.class);

	}


}
