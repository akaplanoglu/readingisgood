package com.getir.readinggood.service.impl;

import com.getir.readinggood.model.Book;
import com.getir.readinggood.model.Order;
import com.getir.readinggood.repository.OrderRespository;
import com.getir.readinggood.service.BookService;
import com.getir.readinggood.service.OrderService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class is for order implementation of book.
 *
 * @author akkapla
 */
@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRespository orderRespository;

	@Autowired
	BookService bookService;

	@Override
	@Transactional
	public Order save(Order order) throws Exception {

		// This is locked so multiple threads can not modify stock at the same time.
		Book stock = bookService.updateStock(order);

		// if stock is not available then book is null
		if (Objects.nonNull(stock)) {
			Book book = order.getBook();
			order.setOrderTotalPrice(book.getPrice().multiply(new BigDecimal(order.getOrderCount())));
			order = orderRespository.save(order);
			return order;
		} else {
			throw new Exception("Stock is not enough");
		}
	}

	@Override
	public Order findById(String orderId) {
		Optional<Order> optionalOrder = orderRespository.findById(orderId);
		return optionalOrder.orElse(null);
	}

	@Override
	public List<Order> findOrdersByDate(Date startDate, Date endDate) {
		return orderRespository.findByCreatedDateBetween(startDate, endDate);
	}

}
