package com.getir.readinggood.service.impl;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import com.getir.readinggood.dto.response.UserOrderMonthlyStatsResponse;
import com.getir.readinggood.model.Order;
import com.getir.readinggood.service.StatisticService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

@Service
public class StatisticServiceImpl implements StatisticService {

	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public List<UserOrderMonthlyStatsResponse> getUsersLastNMonthStats(String userId) {

		Criteria criteria = Criteria.where("userId").is(userId);
		MatchOperation matchOperation = match(criteria);

		ProjectionOperation projectionOperation = project()
				.and("orderCount").as("orderCount")
				.and("orderTotalPrice").as("orderTotalPrice")
				.andExpression("month(createdDate)").as("month")
				.andExpression("year(createdDate)").as("year");

		GroupOperation groupBy = group("year", "month")
				.sum("orderCount").as("totalPurchasedBookCount")
				.sum("orderTotalPrice").as("totalPurchasedAmount")
				.count().as("totalOrderCount");

		SortOperation sortByYearMonth = sort(Sort.by(Direction.ASC, "year", "month"));

		Aggregation agg = newAggregation(
				matchOperation,
				projectionOperation,
				groupBy,
				project("totalPurchasedBookCount", "totalPurchasedAmount", "totalOrderCount", "year", "month"),
				sortByYearMonth
		);

		AggregationResults<UserOrderMonthlyStatsResponse> results = mongoTemplate.aggregate(agg,
				Order.class, UserOrderMonthlyStatsResponse.class
		);

		return results.getMappedResults();

	}

}
