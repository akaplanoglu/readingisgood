package com.getir.readinggood.service;

import com.getir.readinggood.model.Book;
import com.getir.readinggood.model.Order;

public interface BookService {

	Book save(Book book);

	Book updateStock(Order order);

}
