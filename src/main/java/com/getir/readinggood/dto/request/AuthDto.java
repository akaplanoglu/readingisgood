package com.getir.readinggood.dto.request;

import lombok.Data;

@Data
public class AuthDto {

	private String email;

	private String password;

}
