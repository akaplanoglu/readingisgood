package com.getir.readinggood.dto.response;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class UserOrderMonthlyStatsResponse {

	private int year;
	private int month;
	private int totalOrderCount;
	private int totalPurchasedBookCount;
	private BigDecimal totalPurchasedAmount;

}
