package com.getir.readinggood.exception;

import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiError {

	private int status;


	private List<String> errors;

	public ApiError(int status, List<String> errors) {
		super();
		this.status = status;
		this.errors = errors;
	}

	public ApiError(int status, String error) {
		super();
		this.status = status;
		errors = Arrays.asList(error);
	}

}
