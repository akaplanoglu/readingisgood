package com.getir.readinggood.service;

import com.getir.readinggood.dto.response.UserOrderMonthlyStatsResponse;
import com.getir.readinggood.model.Book;
import com.getir.readinggood.model.Order;
import com.getir.readinggood.model.constant.OrderStatus;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StatisticServiceImplTest {


	@Autowired
	StatisticService statisticService;

	@Autowired
	MongoTemplate mongoTemplate;

	private Book book;

	private Order order;

	@Autowired
	OrderService orderService;

	@BeforeAll
	void setUp() throws Exception {

		order = new Order();
		order.setOrderCount(10);
		order.setOrderStatus(OrderStatus.NEW);
		book = new Book();
		book.setPrice(new BigDecimal("5.5"));
		book.setStockCount(90);
		book.setName("Test");

		book = mongoTemplate.save(book);

		order.setBook(book);

		order.setUserId("Test");

		order = orderService.save(order);

		Order order1 = new Order();
		order1.setOrderCount(3);
		order1.setOrderStatus(OrderStatus.NEW);

		order1.setBook(book);

		order1.setUserId("Test");

		orderService.save(order1);

	}

	@Test
	void save() {
		List<UserOrderMonthlyStatsResponse> usersLastNMonthStats = statisticService.getUsersLastNMonthStats("Test");

		Assertions.assertEquals(1, usersLastNMonthStats.size());
		UserOrderMonthlyStatsResponse userOrderMonthlyStatsResponse = usersLastNMonthStats.get(0);
		Assertions.assertEquals(2, userOrderMonthlyStatsResponse.getTotalOrderCount());
		Assertions.assertEquals(13, userOrderMonthlyStatsResponse.getTotalPurchasedBookCount());
		Assertions.assertEquals(new BigDecimal("71.5"), userOrderMonthlyStatsResponse.getTotalPurchasedAmount());

	}

	@AfterAll
	void destroy() {
		mongoTemplate.dropCollection(Order.class);
		mongoTemplate.dropCollection(Book.class);
	}

}
