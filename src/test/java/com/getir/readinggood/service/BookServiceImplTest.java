package com.getir.readinggood.service;

import com.getir.readinggood.model.Book;
import com.getir.readinggood.model.Order;
import com.getir.readinggood.repository.BookRepository;
import java.math.BigDecimal;
import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BookServiceImplTest {

	@Autowired
	BookService bookService;

	private Book book;

	@Autowired
	BookRepository bookRepository;

	@BeforeAll
	void setUp() {

		book = new Book();
		book.setPrice(new BigDecimal("5.5"));
		book.setStockCount(70);
		book.setName("Test");

	}

	@Test
	@org.junit.jupiter.api.Order(1)
	void save() {
		book = bookService.save(book);

		Assertions.assertNotNull(book.getId());
	}

	@Test
	@org.junit.jupiter.api.Order(2)
	void updateStock() {

		Order order = new Order();
		order.setBook(book);
		order.setOrderCount(5);
		bookService.updateStock(order);

		Optional<Book> byId = bookRepository.findById(book.getId());

		Assertions.assertEquals(65, byId.map(Book::getStockCount).orElse(0));

	}

	@AfterAll
	void destroy() {
		bookRepository.deleteAll();
	}


}