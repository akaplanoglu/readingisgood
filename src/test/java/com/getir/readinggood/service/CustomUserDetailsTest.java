package com.getir.readinggood.service;

import com.getir.readinggood.model.Role;
import com.getir.readinggood.model.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CustomUserDetailsTest {

	@Autowired
	CustomUserDetailsService customUserDetailsService;

	private User user;

	@Autowired
	MongoTemplate mongoTemplate;

	@BeforeAll
	void setUp() {
		user = new User();
		user.setEnabled(true);
		user.setUsername("test");
		user.setEmail("test@test.com");
		user.setPassword("12345");


	}

	@Test
	@org.junit.jupiter.api.Order(1)
	void saveUser() {

		customUserDetailsService.saveUser(user);
		Assertions.assertNotNull(user.getId());
	}

	@Test
	@org.junit.jupiter.api.Order(2)
	void loadUserByUsername() {

		UserDetails userDetails = customUserDetailsService.loadUserByUsername("test@test.com");
		Assertions.assertEquals("test@test.com", userDetails.getUsername());
	}


	@AfterAll
	void destroy() {
		mongoTemplate.dropCollection(Role.class);
		mongoTemplate.dropCollection(User.class);
	}


}