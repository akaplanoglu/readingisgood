package com.getir.readinggood.service;

import com.getir.readinggood.model.Book;
import com.getir.readinggood.model.Order;
import com.getir.readinggood.model.constant.OrderStatus;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OrderServiceImplTest {

	@Autowired
	OrderService orderService;

	@Autowired
	MongoTemplate mongoTemplate;

	private Book book;

	private Order order;

	@BeforeAll
	void setUp() {

		order = new Order();
		order.setOrderCount(10);
		order.setOrderStatus(OrderStatus.NEW);
		book = new Book();
		book.setPrice(new BigDecimal("5.5"));
		book.setStockCount(90);
		book.setName("Test");

		book = mongoTemplate.save(book);

		order.setBook(book);
	}

	@Test
	@org.junit.jupiter.api.Order(1)
	void save() throws Exception {
		order = orderService.save(order);

		Assertions.assertEquals(new BigDecimal("55.0"), order.getOrderTotalPrice());
	}

	@Test
	@org.junit.jupiter.api.Order(2)
	void getById() {
		order = orderService.findById(order.getId());

		Assertions.assertNotNull(order);
	}

	@Test
	@org.junit.jupiter.api.Order(3)
	void findOrdersByDate() {
		Instant now = Instant.now(); //current date
		Instant before = now.minus(Duration.ofDays(10));
		Date startDate = Date.from(before);

		Instant after = now.minus(Duration.ofDays(-10));
		Date endDate = Date.from(after);

		List<Order> orders = orderService.findOrdersByDate(startDate, endDate);

		Assertions.assertEquals(1, orders.size());
	}

	@AfterAll
	void destroy() {
		mongoTemplate.dropCollection(Order.class);
	}

}
