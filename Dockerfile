FROM openjdk:8-jdk-alpine
EXPOSE 8080
MAINTAINER akin.kaplanoglu
COPY target/readingisgood.jar readingisgood.jar
ENTRYPOINT ["java","-jar","/readingisgood.jar"]